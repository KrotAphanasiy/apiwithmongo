using ApiWithMongo.Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using ApiWithMongo.Data;

namespace ApiWithMongo.Services
{
    public class BooksService
    {
        private readonly IMongoCollection<Book> _books;

        public BooksService(IBookstoreDbSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _books = database.GetCollection<Book>(settings.BooksCollectionName);
        }

        public List<Book> Get() =>
            _books.Find(book => true).ToList();

        public Book Get(string id) =>
            _books.Find<Book>(book => book.Id == id).FirstOrDefault();
        

    }
}